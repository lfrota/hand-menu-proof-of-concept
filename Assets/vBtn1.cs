﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class vBtn1: MonoBehaviour, IVirtualButtonEventHandler
{

    public GameObject vbBtnObj;
    public string btnName;

    // Use this for initialization
    void Start()
    {
        vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Debug.Log(btnName + " pressed");
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Debug.Log(btnName + " released");
    }
}