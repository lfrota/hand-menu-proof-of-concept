﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {

    private Collider col;
    private Color originalColor;
    private bool triggered;
    public GameObject main;
    public int idNum;

	void Start () {
        col = this.gameObject.GetComponent<SphereCollider>();
        originalColor = this.gameObject.GetComponent<Renderer>().material.color;
	}
    private void OnTriggerEnter(Collider other) {
        triggered = true;
        ButtonBehavior(idNum);
        this.gameObject.GetComponent<Renderer>().material.color = Color.Lerp(originalColor, Color.red, 0.5f);
    }

    private void OnTriggerExit(Collider other) {
        this.gameObject.GetComponent<Renderer>().material.color = originalColor;
        triggered = false;
    }

    private void ButtonBehavior(int selector)
    {
        Debug.Log(selector);



    }

   /* private void Update()
    {
        if(triggered)
            this.gameObject.GetComponent<Renderer>().material.color = Color.Lerp(originalColor, Color.red, 2.0f);
    }*/
}
