﻿using UnityEngine;

public class PlaneFaceCamera : MonoBehaviour
{
    public Camera cam;

    void Start()
    {
        float pos = (cam.nearClipPlane + 0.01f);

        transform.position = cam.transform.position + cam.transform.forward * pos;
        transform.LookAt(cam.transform);
        transform.Rotate(90.0f, 0.0f, 0.0f);

        float h = (Mathf.Tan(cam.fieldOfView * Mathf.Deg2Rad * 0.5f) * pos * 2f) / 10.0f;

        transform.localScale = new Vector3(h * cam.aspect, 1.0f, h);
    }
}